const routes = [{
    path: '/',
    component: () => import('pages/login.vue'),
    name: "login"
  },
  {
    path: '/forgotpassword',
    component: () => import('pages/forgotpassword.vue'),
    name: 'forgotPassword'
  },
  {
    path: '/register',
    component: () => import('pages/register.vue'),
    name: 'register'
  },
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [{
        path: 'usersetting',
        component: () => import('pages/user_setting.vue'),
        name: 'usersetting'
      },
      {
        path: '/expressionlist',
        component: () => import('pages/expressionlist.vue'),
        name: 'expressionlist'
      },
      {
        path: '/expression/:key',
        component: () => import('pages/expression.vue'),
        name: 'expression'
      },
      {
        path: '/infosetting/',
        component: () => import('pages/infosetting_mobile.vue'),
        name: 'infosetting'
      },
      {
        path: '/dialoglist/',
        component: () => import('pages/dialoglist.vue'),
        name: 'dialoglist'
      },
      {
        path: '/dialog/:key',
        component: () => import('pages/dialog.vue'),
        name: 'dialog'
      },
      {
        path: '/vocabulary',
        component: () => import('pages/vocabulary.vue'),
        name: 'vocabulary'
      }
    ]
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
